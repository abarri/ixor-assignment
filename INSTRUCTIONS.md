# Welcome to the Ixor Challenge!

Are you ready to put your clustering skills to the test? In this challenge, we're looking for the best algorithm for clustering information blobs on structured documents, such as invoices.

## The Problem

Have you ever looked at a bunch of invoices, receipts, or other structured documents? Without reading a word, you can already guess where certain types of information are located on the page. This is because the information is logically grouped into clusters, or "information blobs."

When we process a document with our model, we can't just sort all the words from top to bottom and from left to right, because we would mix the information from different clusters. It's important to properly group the information into clusters in order to correctly process the document.

## The Challenge

Your task is to design an algorithm that can automatically group information blobs on structured documents.

The input is a `content.json` file containing all the words of the document. 

The output should be the same `content.json` file, but with a cluster number assigned to each word, like this:

``word = {"word": "first", "position": [x1, y1, x2, y2], "cluster": 1}``

An example of the different informationclusters in a document:
![](example_clusters.jpg)

## Tip
You can use the `plot_clusters.py` file to visualize the clusters that your algorithm has identified.

## Evaluation

During the interview, we will review your code and discuss your approach to solving the challenge. We will evaluate your algorithm based on:

- The accuracy of the clusters (how well the algorithm groups similar blobs together)
- The efficiency of the algorithm (how quickly it can process the data)

## Getting Started

Ready to get started? Download the data file and start working on your algorithm! We can't wait to see what you come up with. Good luck, and happy clustering!