# %% [markdown]
# # Task

# %% [markdown]
# Have you ever looked at a bunch of invoices, receipts, or other structured documents? Without reading a word, you can already guess where certain types of information are located on the page. This is because the information is logically grouped into clusters, or "information blobs."
#
# When we process a document with our model, we can't just sort all the words from top to bottom and from left to right, because we would mix the information from different clusters. It's important to properly group the information into clusters in order to correctly process the document.
#
# Our task is to design an algorithm that can automatically group information blobs on structured documents.
# The input is a `content.json` file containing all the words of the document. The output should be the same `content.json` file, but with a cluster number assigned to each word, like this:
# ```word = {"word": "first", "position": [x1, y1, x2, y2], "cluster": 1}```

# %% [markdown]
# # Approach

# %% [markdown]
# This task could be readily solved using Python packages for [document layout analysis](https://en.wikipedia.org/wiki/Document_layout_analysis), such as [ocropus3](https://github.com/NVlabs/ocropus3-ocroseg) and [layout-parser](https://layout-parser.github.io/). Since the problem concerns invoices it is also worth exploring packages for detecting tables, such as [CascadeTabNet](https://github.com/DevashishPrasad/CascadeTabNet).
#
# For real-world applications I would first use these open-source packages and take advantage of the trained deep-learning-models on which they are based. If these packages do not meet specific application requirements then there might still be a possibility to extend these packages (e.g. by partially retraining the models). In any case I would be very careful not to reinvent the wheel.
#
# Since this is a technical assessment I decided to show my programming & machine learning skills by implementing an algorithm from scratch based on some relevant computer vision and machine learning algorithms. These algorithms are explained in the section below.
#
# The last section of this document contains my implementation.

# %% [markdown]
# # Relevant algorithms

# %% [markdown]
# ## Machine Learning: Clustering

# %% [markdown]
# This section describes four popular types of clustering algorithms following this [towardsdatascience article](https://towardsdatascience.com/the-5-clustering-algorithms-data-scientists-need-to-know-a36d136ef68).
#
# For each clustering algorithm, I explain the benefits/drawbacks for this assignment.

# %% [markdown]
# ### K-means
#
# ![](https://miro.medium.com/v2/resize:fit:640/1*KrcZK0xYgTa4qFrVr0fO2w.gif)
# <center><figcaption><b>Figure. </b>K-means clustering with k=3</figcaption><br></center>
#
# The K-means algorithm in data mining starts with a first group of randomly selected centroids, which are used as the beginning points for every cluster, and then performs iterative (repetitive) calculations to optimize the positions of the centroids. The number of centroids (and the resulting clusters) is controlled by the parameter `k`.
#
# The main disadvantage is that the clusters have a circular shape, while text blobs in documents have a rectangular shape.

# %% [markdown]
# ### Gaussian Mixture Model clustering
#
# ![](https://miro.medium.com/v2/resize:fit:640/1*OyXgise21a23D5JCss8Tlg.gif)
# <center><figcaption><b>Figure. </b>Fitting a Gaussian Mixture Model using the Expectation Maximization algorithm</figcaption><br></center>
#
# Gaussian Mixture Models (GMMs) give us more flexibility than K-Means. 
# With GMMs we assume that the data points are Gaussian distributed.
# This is a less restrictive assumption than saying they are circular by using the mean.
#

# %% [markdown]
# ### Density based clustering (mean-shift & DBSCAN)
#
# ![](https://miro.medium.com/v2/resize:fit:640/1*vyz94J_76dsVToaa4VG1Zg.gif)
# <center><figcaption><b>Figure. </b>Mean-shift clustering</figcaption><br></center>
#
# Density-based clustering algorithms such as mean-shift and DBSCAN try to find the most dense regions in the data.
#
# As a disadvantage in the context of the this assignment density-based clustering algorithms have difficulties detecting clusters of varying density, which occurs when text documents use different font sizes.

# %% [markdown]
# ### Hierarchical clustering
#
# ![](https://miro.medium.com/v2/resize:fit:4800/1*ET8kCcPpr893vNZFs8j4xg.gif)   
# <center><figcaption><b>Figure. </b>Visualizing agglomerative clustering with a dendogram.</figcaption><br></center>
#
# This type of algorithms makes a lot of sense seen the hierarchical layout of text documents: letters form words, words form lines, lines form paragraphs, paragraphs form text columns, etc.
#
# There are two strategies: top-down (divisive) & bottom-up (agglomerative), where the last one is more popular.
#
# For this assignment, I decided to use the Agglomerative Clustering algorithm of the scikit-learn package.

# %% [markdown]
# ## Computer Vision: Morphological Operators
#
# Morphological operators such as dilation and erosion can be used to connect words to text blobs. 
#
# https://medium.com/hackernoon/an-introduction-to-morphological-operations-for-digital-image-text-classification-79cb14bab2d7
#
# ![](https://miro.medium.com/v2/resize:fit:720/format:webp/1*P4crKj1LLEH30VBHRZUYaQ.png)
# <center><figcaption><b>Figure. </b>Original image</figcaption><br></center>
#
#
# ![](https://miro.medium.com/v2/resize:fit:720/format:webp/1*CGLAzEU4Eq7YpJpeEmHUfg.png "Test")
# <center><figcaption><b>Figure. </b>Results of Horizontal Dilation (a) and subsequent Erosion (b)</figcaption><br></center>
#
#
# ![](https://miro.medium.com/v2/resize:fit:720/format:webp/1*8r6hK-pP-odoe2P1B1ZWNQ.png)
# <center><figcaption><b>Figure. </b> Results of Vertical Dilation (a) and subsequent Erosion (b)</figcaption><br></center>
#

# %% [markdown]
# # Implementation

# %% [markdown]
# ## Overview

# %% [markdown]
# **Proposed model:**
#
#  - Merge words into lines by fitting an Agglomerative Clustering algorithm on the y-values 
#  - Cut the lines where the words are too far apart
#  - Merge horizontally aligned lines
#  - Merge overlapping text fragments
#
# **Possible extensions:**
#     
#  - Visual cues (e.g. detect table lines using the Hough transform, different fonts)
#  - Logical layout analysis on top of geometric layout analysis

# %% [markdown]
# ## Code structure

# %% [markdown]
# The code is structured into modules that are located in the `src` folder.
#
# The most important modules are listed below:
#  
#  - `data_fetching.py` for reading the json files
#  - `data_processing.py` for converting the json files to a table
#  - `modeling.py` for clustering the words into lines and then into text fragments
#  - `visualization.py` for showing the words, line boxes and fragment boxes 
#  - `data_serialization.py` for writing the clustered data back to json files

# %% [markdown]
# ## Demo

# %%
import __init__

# %% [markdown]
# ### Data fetching

# %%
from src.constants import DATA_PATH
from src.data_fetching import fetch_doc_records

# %%
doc_records = fetch_doc_records(json_folder= DATA_PATH / "in/json")

# %% [markdown]
# ### Data processing

# %%
from src.data_processing import TextBoxTable

# %%
word_box_table = TextBoxTable.from_doc_records(doc_records)

# %%
word_box_table.head(60)

# %% [markdown]
# ### Modeling

# %%
from sklearn.cluster import AgglomerativeClustering

from src.modeling import WordClusterModel

# %%
cluster_algorithm =  AgglomerativeClustering(
    n_clusters=None,
    metric="euclidean",
    linkage="ward",
    distance_threshold=0.01
)
model = WordClusterModel(
    cluster_algorithm,
    max_chars_between_words_in_lines=3,
    max_char_deviation_between_aligned_lines=1,
    max_empty_lines_in_aligned_text=7,
)

# %%
line_box_table = model.assign_line_clusters(word_box_table)
fragment_box_table = model.assign_fragment_clusters(line_box_table)

# %% [markdown]
# ### Visualization

# %%
import cv2
from matplotlib import pyplot as plt

from src.constants import DATA_PATH
from src.visualization import DocImage 

# %%
fragment_box_table

# %%
doc_image = DocImage()
doc_image.draw_text(word_box_table, hex_color="#000000")
doc_image.draw_borders(line_box_table, hex_color="#4ca832")
doc_image.draw_borders(fragment_box_table, hex_color="#4336d6")

# %%
for (doc_id, page), img in doc_image.items():
    plt.figure(figsize=(img.shape[0] / 50, img.shape[1] / 50))
    plt.imshow(img)
    plt.title(f"{doc_id}_page_{page}")

# %%
png_folder = DATA_PATH / "out/png"
png_folder.mkdir(parents=True, exist_ok=True)

for (doc_id, page), img in doc_image.items():
    path = png_folder / f"{doc_id}_page_{page}.png"
    cv2.imwrite(str(path), cv2.cvtColor(img, cv2.COLOR_RGB2BGR))

# %% [markdown]
# ### Data serialization

# %%
from src.constants import DATA_PATH
from src.data_serialization import serialize_text_box_table

# %%
serialize_text_box_table(
    fragment_box_table, 
    in_json_folder = DATA_PATH / "in/json",
    out_json_folder = DATA_PATH / "out/json",
)
