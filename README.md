You can find my assignment in `notebooks/word_clustering.py`.

This file can be opened as a jupyter notebook and will be automatically synced with
a `.ipynb` file if `jupytext` is installed.