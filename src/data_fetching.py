import json
from pathlib import Path
from typing import Any, Dict, List

from src.constants import DEFAULT_PAGE_HEIGHT, DEFAULT_PAGE_WIDTH


def fetch_doc_records(json_folder: Path) -> List[Dict[str, Any]]:
    doc_records: List[Dict[str, Any]] = []
    for path in json_folder.rglob("*.json"):
        loaded_json = json.loads(path.read_text())
        default_record = dict(
            doc_id=loaded_json["ID"].removesuffix(".pdf"),
            height=DEFAULT_PAGE_HEIGHT,
            width=DEFAULT_PAGE_WIDTH,
            cluster=-1,
        )
        doc_records.extend(
            [
                default_record | raw_record | {"textbox_idx": idx}
                for idx, raw_record in enumerate(loaded_json["content"])
            ]
        )
    return doc_records
