from pathlib import Path

PROJECT_PATH: Path = Path(__file__).resolve().parents[1]
DATA_PATH: Path = PROJECT_PATH / "data"

DEFAULT_PAGE_HEIGHT: int = 1754
DEFAULT_PAGE_WIDTH: int = 1240
