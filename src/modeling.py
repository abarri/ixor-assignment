from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any, Callable, Dict, Self

import numpy as np
import pandas as pd

from src.data_processing import TextBoxTable


class ClusterAlgorithmABC(ABC):
    @abstractmethod
    def fit(self, X: np.ndarray) -> Self:
        ...

    @property
    @abstractmethod
    def labels_(self) -> np.ndarray:
        ...


@dataclass
class WordClusterModel:
    cluster_algorithm: ClusterAlgorithmABC
    max_chars_between_words_in_lines: int = 3
    max_char_deviation_between_aligned_lines: int = 1
    max_empty_lines_in_aligned_text: int = 7

    def assign_line_clusters(self, word_box_table: TextBoxTable) -> TextBoxTable:
        dfs = []
        groups = word_box_table.groupby(["doc_id", "page", "height"]).groups  # type: ignore
        for (_, _, height), idxs in groups.items():
            df = word_box_table.loc[idxs].reset_index(drop=True)
            y_data = df[["min_y", "max_y"]].values / height
            df["cluster"] = self.cluster_algorithm.fit(y_data).labels_
            df["char_width"] = (df.max_x - df.min_x).clip(lower=1) / df.text.str.len()
            df = df.sort_values(by=(["cluster", "min_x"])).reset_index(drop=True)
            idx_to_cluster = self._split_clusters(df, self._split_criterion_lines)
            df["cluster"] = df.index.map(idx_to_cluster)
            dfs.append(df)
        df = pd.concat(dfs, ignore_index=True)
        line_box_table = TextBoxTable.apply_schema(df)
        return line_box_table

    def assign_fragment_clusters(self, line_box_table: TextBoxTable) -> TextBoxTable:
        dfs = []
        groups = line_box_table.groupby(["doc_id", "page"]).groups  # type: ignore
        for _, idxs in groups.items():
            df = line_box_table.loc[idxs].reset_index(drop=True)
            df["char_width"] = (df.max_x - df.min_x).clip(lower=1) / df.text.str.len()
            df = (
                df.assign(text_len=df.text.str.len())
                .groupby(["doc_id", "cluster"])
                .agg(
                    dict(
                        textbox_idx=list,
                        min_x=min,
                        max_x=max,
                        min_y=min,
                        max_y=max,
                        char_width="mean",
                    )
                )
                .reset_index()
            )
            df["line_height"] = (df.max_y - df.min_y).clip(lower=1)
            pos_cols = ["min_y", "max_y", "min_x", "max_x"]
            df = df.sort_values(by=pos_cols).reset_index(drop=True)
            idx_to_cluster = self._merge_clusters(
                df, self._merge_criterion_aligned_lines
            )
            df["cluster"] = df.index.map(idx_to_cluster)
            i = 0
            while True:
                i += 1
                df = (
                    df.groupby(["doc_id", "cluster"])
                    .agg(
                        dict(
                            textbox_idx="sum",
                            min_x=min,
                            max_x=max,
                            min_y=min,
                            max_y=max,
                        )
                    )
                    .reset_index()
                )
                df = df.sort_values(by=pos_cols).reset_index(drop=True)
                idx_to_cluster = self._merge_clusters(
                    df, self._merge_criterion_overlapping_boxes
                )
                updated_clusters = df.index.map(idx_to_cluster)
                if (updated_clusters == df.cluster).all():
                    break
                df["cluster"] = updated_clusters
            dfs.append(df)
        df = (
            pd.concat(dfs, ignore_index=True)[["doc_id", "textbox_idx", "cluster"]]
            .explode("textbox_idx", ignore_index=True)
            .merge(
                line_box_table.drop(columns=["cluster"]),
                on=["doc_id", "textbox_idx"],
                validate="one_to_one",
            )
        )
        fragment_box_table = TextBoxTable.apply_schema(df)
        return fragment_box_table

    def _split_criterion_lines(self, row1: pd.Series, row2: pd.Series) -> bool:
        if row1.cluster != row2.cluster:
            return True
        avg_char_width = (row1.char_width + row2.char_width) / 2
        max_width = self.max_chars_between_words_in_lines * avg_char_width
        is_close_enough = (row2.min_x - row1.max_x) <= max_width
        return not is_close_enough

    def _merge_criterion_aligned_lines(self, row1: pd.Series, row2: pd.Series) -> bool:
        avg_line_height = (row1.line_height + row2.line_height) / 2
        assert row2.min_y >= row1.min_y
        if (
            row2.min_y - row1.min_y
        ) > self.max_empty_lines_in_aligned_text * avg_line_height:
            return False
        avg_char_width = (row1.char_width + row2.char_width) / 2
        max_width = self.max_char_deviation_between_aligned_lines * avg_char_width
        is_left_aligned = abs(row2.min_x - row1.min_x) <= max_width
        is_right_aligned = abs(row2.max_x - row1.max_x) <= max_width
        return is_left_aligned or is_right_aligned

    def _merge_criterion_overlapping_boxes(
        self, row1: pd.Series, row2: pd.Series
    ) -> bool:
        if row2.min_y > row1.max_y:
            return False
        if (row1.min_x <= row2.max_x) and (row1.max_x >= row2.min_x):
            return True
        if (row2.min_x <= row1.max_x) and (row2.max_x >= row1.min_x):
            return True
        return False

    @staticmethod
    def _split_clusters(
        df: pd.DataFrame,
        split_criterion: Callable[[pd.Series, pd.Series], bool],
    ) -> Dict[int, int]:
        idx_to_cluster = dict()
        last_cluster = 0
        prev_row = None
        for idx, row in enumerate(df.itertuples()):
            if prev_row is not None and split_criterion(prev_row, row):
                last_cluster += 1
            prev_row = row
            idx_to_cluster[idx] = last_cluster
        return idx_to_cluster

    @staticmethod
    def _merge_clusters(
        df: pd.DataFrame,
        merge_criterion: Callable[[pd.Series, pd.Series], bool],
    ) -> Dict[int, int]:
        next_cluster = 0
        idx_to_cluster = dict()
        cluster_to_idxs = dict()
        for idx1 in range(df.shape[0]):
            row1 = df.iloc[idx1]
            if idx1 not in idx_to_cluster:
                idx_to_cluster[idx1] = next_cluster
                cluster_to_idxs[next_cluster] = {idx1}
                next_cluster += 1
            for idx2 in range(idx1 + 1, df.shape[0]):
                row2 = df.iloc[idx2]
                try:
                    if not merge_criterion(row1, row2):
                        continue
                except StopIteration:
                    break
                cluster1 = idx_to_cluster[idx1]
                cluster2 = idx_to_cluster.setdefault(idx2, cluster1)
                min_cluster, max_cluster = sorted([cluster1, cluster2])
                cluster_to_idxs[min_cluster] |= {idx1, idx2}
                if min_cluster < max_cluster:
                    for idx in cluster_to_idxs[max_cluster]:
                        idx_to_cluster[idx] = min_cluster
                    cluster_to_idxs[min_cluster] |= cluster_to_idxs.pop(max_cluster)
        idx_to_cluster = {
            new_idx: idx_to_cluster[old_idx]
            for old_idx, new_idx in enumerate(sorted(idx_to_cluster))
        }
        return idx_to_cluster
