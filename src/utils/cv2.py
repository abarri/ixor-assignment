from dataclasses import dataclass, field

import cv2
import numpy as np
import PIL as pil
from ofunctions.bisection import bisect


@dataclass(frozen=True)
class TextBox:
    min_x: int
    max_x: int
    min_y: int
    max_y: int
    text: str
    thickness: int = 1
    font_face: int = cv2.FONT_HERSHEY_COMPLEX
    font_hex_color: str = "#000000"
    border_hex_color: str = "#000000"
    padding: float = 0.1
    font_scale: float = field(init=False)

    def __post_init__(self):
        self.__dict__["font_scale"] = self._compute_font_scale()

    @property
    def width(self) -> int:
        return max([self.max_x - self.min_x, 1])

    @property
    def height(self) -> int:
        return max([self.max_y - self.min_y, 1])

    def _compute_font_scale(self) -> float:
        max_scale = cv2.getFontScaleFromHeight(
            self.font_face, self.height, self.thickness
        )

        def check_if_scale_is_small_enough(scale: float) -> bool:
            (width, height_without_baseline), baseline = cv2.getTextSize(
                self.text, self.font_face, scale, self.thickness
            )
            height = height_without_baseline + baseline
            return (width <= self.width) and (
                (1 + self.padding) * height <= self.height
            )

        return bisect(
            check_if_scale_is_small_enough,
            np.linspace(0, max_scale, 100),
            allow_all_expected=True,
        )

    def put_text(self, img: np.ndarray) -> np.ndarray:
        (text_width, text_height_without_baseline), baseline = cv2.getTextSize(
            self.text, self.font_face, self.font_scale, self.thickness
        )
        text_height = text_height_without_baseline + baseline
        offset_x = (self.width - text_width) // 2
        offset_y = -baseline - (self.height - text_height) // 2
        return cv2.putText(
            img,
            self.text,
            (self.min_x + offset_x, self.max_y + offset_y),
            self.font_face,
            self.font_scale,
            pil.ImageColor.getcolor(self.font_hex_color, "RGB"),  # type: ignore
            self.thickness,
        )

    def put_border(self, img: np.ndarray) -> np.ndarray:
        return cv2.rectangle(
            img,
            (self.min_x, self.max_y),
            (self.max_x, self.min_y),
            pil.ImageColor.getcolor(self.border_hex_color, "RGB"),  # type: ignore
            self.thickness,
        )
