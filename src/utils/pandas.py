from typing import Any, Dict, Self, cast, get_type_hints

import pandas as pd
from strictly_typed_pandas.create_empty_dataframe import create_empty_dataframe
from strictly_typed_pandas.dataset import DataSetBase
from strictly_typed_pandas.validate_schema import validate_schema


class TableBase(DataSetBase):
    @classmethod
    def apply_schema(cls, df: pd.DataFrame) -> Self:
        col_to_dtype = cls()._schema_annotations
        df = df[list(col_to_dtype)].astype(col_to_dtype)
        return cls(df)

    @property
    def _schema_annotations(self) -> Dict[str, Any]:
        return get_type_hints(self)

    def _continue_initialization(self):
        if self.shape == (0, 0):
            df = create_empty_dataframe(self._schema_annotations)
            super().__init__(df)
        else:
            schema_observed = cast(Dict[str, Any], dict(zip(self.columns, self.dtypes)))
            validate_schema(self._schema_annotations, schema_observed)
