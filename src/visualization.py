from typing import Dict, Iterable, Tuple

import numpy as np
from unidecode import unidecode

from src.data_processing import TextBoxTable
from src.utils.cv2 import TextBox


class DocImage:
    def __init__(self):
        self.doc_page_to_text_box_img: Dict[Tuple[str, int], np.ndarray] = dict()

    def __getitem__(self, doc_id: str, page: int) -> np.ndarray:
        return self.doc_page_to_text_box_img[(doc_id, page)]

    def items(self) -> Iterable:
        return self.doc_page_to_text_box_img.items()

    def draw_text(self, table: TextBoxTable, hex_color: str = "#000000"):
        self.draw_text_boxes(
            table, font_hex_color=hex_color, put_text=True, put_border=False
        )

    def draw_borders(self, table: TextBoxTable, hex_color: str = "#000000"):
        self.draw_text_boxes(
            table, border_hex_color=hex_color, put_text=False, put_border=True
        )

    def draw_text_boxes(
        self,
        table: TextBoxTable,
        font_hex_color: str = "#000000",
        border_hex_color: str = "#000000",
        put_text: bool = True,
        put_border: bool = True,
    ) -> None:
        group_cols = ["doc_id", "page", "height", "width", "cluster"]
        groups = table.groupby(group_cols).groups  # type: ignore
        for (doc_id, page, height, width, _), idxs in groups.items():
            img = self.doc_page_to_text_box_img.setdefault(
                (doc_id, page), np.full((height, width, 3), 255, dtype=np.uint8)
            )
            df = (
                table.loc[idxs]
                .sort_values(by=["max_y", "min_x", "min_y", "max_x"])
                .reset_index(drop=True)
            )
            text_box = TextBox(
                min_x=df.min_x.min(),
                max_x=df.max_x.max(),
                min_y=df.min_y.min(),
                max_y=df.max_y.max(),
                text=unidecode(" ".join(df.text)),
                font_hex_color=font_hex_color,
                border_hex_color=border_hex_color,
            )
            if put_text:
                img = text_box.put_text(img)
            if put_border:
                img = text_box.put_border(img)
            self.doc_page_to_text_box_img[(doc_id, page)] = img
