from typing import Any, Dict, List, Self

import pandas as pd

from src.utils.pandas import TableBase


class TextBoxTable(TableBase):
    doc_id: str
    textbox_idx: int
    page: int
    height: int
    width: int
    text: str
    min_x: int
    max_x: int
    min_y: int
    max_y: int
    cluster: int

    @classmethod
    def from_doc_records(cls, doc_records: Dict[str, List[Dict[str, Any]]]) -> Self:
        df = pd.DataFrame.from_records(doc_records).rename(columns=dict(word="text"))
        box_cols = ["min_x", "min_y", "max_x", "max_y"]
        df[box_cols] = df.pop("position").to_list()
        for col in box_cols:
            assert df[col].between(0, 1).all()
            n_pixels = df.width if col.endswith("_x") else df.height
            df[col] = (df[col] * n_pixels).round().astype(int)
        is_horizontally_oriented = (df.min_x <= df.max_x) & (df.min_y <= df.max_y)
        if not is_horizontally_oriented.all():
            print("Below text boxes are not horizontally oriented and will be dropped:")
            print_cols = ["doc_id", "textbox_idx", "page", "text"]
            print(df.loc[~is_horizontally_oriented, print_cols].to_string(index=False))
        df = df[is_horizontally_oriented].reset_index(drop=True)
        df = df.sort_values(by=["doc_id", "textbox_idx"]).reset_index(drop=True)
        if (df.cluster == -1).all():
            df["cluster"] = df.index
        assert (df.cluster >= 0).all()
        return cls.apply_schema(df)
