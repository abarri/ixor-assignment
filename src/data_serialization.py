import json
from pathlib import Path

from src.data_processing import TextBoxTable


def serialize_text_box_table(
    table: TextBoxTable, in_json_folder: Path, out_json_folder: Path
):
    out_json_folder.mkdir(parents=True, exist_ok=True)
    for doc_id, idxs in table.groupby("doc_id").groups.items():
        loaded_json = json.loads((in_json_folder / f"{doc_id}.json").read_text())
        for idx in idxs:
            row = table.loc[idx]
            content = loaded_json["content"][row.textbox_idx]
            assert content["page"] == row.page
            assert content["word"] == row.text
            content["cluster"] = int(row.cluster)
        dumped_json = json.dumps(loaded_json, indent=4)
        (out_json_folder / f"{doc_id}.json").write_text(dumped_json)
